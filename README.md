This is the third-party Nutonian source repository.

It contains third-party libraries we use to build the Nutonian code,
but whose build systems are not part of our main repo.

Our actual source tree contains a directory called tparty which
contains the third-party code which we build from within our overall
build system.


Version Information:

1. OpenSSL 1.0.1g for Windows. Installed from  http://slproweb.com/download/Win64OpenSSL-1_0_1g.exe and http://slproweb.com/download/Win32OpenSSL-1_0_1g.exe.

1. boost_1_57.tar.bz2
was created by removing all documentation from the original tarball from the boost website

```
find boost_1_57_0 -type d -name "doc" | xargs rm  -rf
tar cvjf boost_1_57_0.tar.bz2 boost_1_57_0
```