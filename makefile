###### Platform specific stuff
UNAME := $(shell uname)

# if that didn't work, try the windows location
ifeq ($(UNAME), )
UNAME := $(shell "wbin\\uname.exe")
endif

MKDIR=mkdir -p
BUNZIP2=bunzip2
GZIP=gzip
B2_BOOTSTRAP=./bootstrap.sh
B2=./b2
TAR=tar
TOUCH=touch
MKDIR=mkdir
CP=cp
RMRF=rm -rf
BOOST_BUILD_DIR=$(shell pwd)/boost-build

B2_WITH:=--with-program_options --with-date_time --with-filesystem --with-regex --with-serialization --with-system --with-thread

# OS Specific stuff (yuk)
ifeq ($(UNAME), Darwin)
	BUILD_FASTER_FLAGS:=-j`sysctl -n hw.ncpu`
	B2_FLAGS = --build-dir=$(BOOST_BUILD_DIR) --build-type=complete --layout=versioned link=static threading=multi variant=release toolset=clang cxxflags="-stdlib=libc++ -arch x86_64 -std=c++0x" linkflags="-stdlib=libc++  -arch x86_64"
	SSL_CONFIG_COMMAND=./Configure  darwin64-x86_64-cc
endif

ifeq ($(UNAME), Linux)
	BUILD_FASTER_FLAGS:=-j`nproc`
	B2_FLAGS = --layout=tagged --build-dir=$(BOOST_BUILD_DIR) --build-type=complete --layout=versioned  link=static threading=multi variant=release toolset=gcc cxxflags=-std=c++0x
	SSL_CONFIG_COMMAND=./config
endif


#######################################################
####### Override appropriately for Windows ############
#######################################################

ifeq ($(UNAME), WindowsNT) # Windows
	BUILD_FASTER_FLAGS:=-j$(NUMBER_OF_PROCESSORS)
	MKDIR=mkdir
	BUNZIP2="wbin\\bunzip2.exe"
	GZIP="wbin\\gzip.exe"
	TAR="wbin\\tar.exe"
	TOUCH="wbin\\touch.exe"
	RMRF="wbin\\rm.exe" -rf
	MKDIR="wbin\mkdir.exe"
	CP="wbin\cp.exe"
	B2=b2.exe
	B2_BOOTSTRAP=".\\bootstrap.bat"
	B2_FLAGS = --build-type=complete link=static threading=multi toolset=msvc stage
	B2_WITH+=$(B2_WITH) --with-chrono
ifeq ($(findstring amd64,$(LIB)), amd64)
	B2_FLAGS+=address-model=64
endif
endif


# This makefile extracts and builds the third-party libraries we use on linux.
INSTALLDIR=$(shell pwd)/install




###############
## Definition of default build targets. Different on windows and mac!
###############

ifeq ($(UNAME), WindowsNT) # Windows
all:  boost_1_57_0/.done
endif

ifeq ($(UNAME), Darwin)
all:   boost_1_57_0/.done  openssl-1.0.1g/.done
endif

ifeq ($(UNAME), Linux)
all:   boost_1_57_0/.done  gcovr qt-everywhere-opensource-src-4.8.6/.done openssl-1.0.1g/.done
endif


$(INSTALLDIR):
	$(MKDIR) "$(INSTALLDIR)"
	$(MKDIR) "$(INSTALLDIR)/include"
	$(MKDIR) "$(INSTALLDIR)/lib"


# note that we remove old versions of boost, potentially
boost_1_57_0/.done:
	$(MAKE) $(INSTALLDIR)
	@echo "Removing previous installations of boost"
	-$(RMRF) install/include/boost install/lib/libboost* /tmp/build-boost $(BOOST_BUILD_DIR)
	@echo "Building boost for platform $(UNAME)"
	@echo " B2_WITH:  $(B2_WITH)"
	@echo " B2_FLAGS: $(B2_FLAGS)"
	@echo "Uncompressing boost...."
	$(BUNZIP2) -c boost_1_57_0.tar.bz2 | $(TAR) x
	@echo "Updating transform_width.hpp ...."
#	$(CP) -f transform_width_boost_1_55_fixed.hpp boost_1_55_0/boost/archive/iterators/transform_width.hpp
	(cd boost_1_57_0 && $(B2_BOOTSTRAP) )
	(cd boost_1_57_0 && $(B2) $(B2_WITH) $(B2_FLAGS) $(BUILD_FASTER_FLAGS) --prefix="$(INSTALLDIR)" install)
	$(TOUCH) boost_1_57_0/.done


# Note that we need the gtk development library installed so we can
# get gtk theme support (so eureqa doesn't look like crap)
#
# Command on ubuntu:
#sudo apt-get install libgtk2.0-dev
#
# Command on centos6:
# sudo yum install gtk2-devel -y
#
# Various  other tools required on centos6:
# sudo yum install make gcc-c++ -y
qt-everywhere-opensource-src-4.8.6/.done:
	$(MAKE) $(INSTALLDIR)
	tar xzf qt-everywhere-opensource-src-4.8.6.tar.gz
	(cd qt-everywhere-opensource-src-4.8.6 && ./configure -platform linux-g++ -static -release -gtkstyle -qt-zlib -qt-libpng -qt-libmng -qt-libtiff -qt-libjpeg -opensource -confirm-license -no-webkit -no-phonon -no-phonon-backend -no-script -no-scripttools -no-qt3support -no-multimedia -no-dbus -nomake demos -nomake tools -nomake examples --prefix=$(INSTALLDIR))
	(cd qt-everywhere-opensource-src-4.8.6 && make $(BUILD_FASTER_FLAGS))
	(cd qt-everywhere-opensource-src-4.8.6 && make install)
	# Don't make gdb indexes -- reduces link time by 10 seconds
	mv $(INSTALLDIR)/mkspecs/features/unix/gdb_dwarf_index.prf $(INSTALLDIR)/mkspecs/features/unix/gdb_dwarf_index.prf.is_a_stupid_feature
	touch qt-everywhere-opensource-src-4.8.6/.done


.PHONY:qt
qt: qt-everywhere-opensource-src-4.8.6/.done

#
# By default QT builds with -MD on windows even if you say -static.
# Thus, We change the makefile to use MT to actually build qt with the MS static c library by copying the .pro file.
# See this link for more info:
# http://qt-project.org/faq/answer/why_does_a_statically_built_qt_use_the_dynamic_visual_studio_runtime_library
#
.PHONY:qtwin
qtwin:
	@echo "Extracting qt for windows..."
	$(GZIP) -d -c qt-everywhere-opensource-src-4.8.6.tar.gz | $(TAR) x
	$(CP) -f msvc32-2012-qmake.conf qt-everywhere-opensource-src-4.8.6\mkspecs\win32-msvc2012\qmake.conf
	@echo "Copying to c:\qt\4.8.6...."
	$(MKDIR) -p c:\qt
	$(CP) -R -f qt-everywhere-opensource-src-4.8.6 c:\qt\4.8.6
	@echo "Building..."
	(c: && cd \qt\4.8.6 && configure -platform win32-msvc2012 -static -debug-and-release -opensource -confirm-license -no-webkit -no-phonon -no-phonon-backend -no-script -no-scripttools -no-qt3support -no-multimedia -no-dbus -nomake demos -nomake tools -nomake examples)
	(c: && cd \qt\4.8.6 && nmake)
	@echo "Build complete. Cleaning..."
	$(RMRF) qt-everywhere-opensource-src-4.8.6
	@echo "***************"
	@echo " Done!"
	@echo " Please add c:\qt\4.8.6\bin to your path"
	@echo "***************"

.PHONY:qtmac
qtmac:
	@echo "Extracting qt for mac..."
	$(GZIP) -d -c qt-everywhere-opensource-src-4.8.6.tar.gz | $(TAR) x
	@echo "Configuring and building..."
	# patch from https://gist.github.com/unixmonkey/985abd25b0f473506a47#file-qt_on_osx_yosimite-patch
	patch -p0 < qt_on_osx_yosimite.patch 
	(cd qt-everywhere-opensource-src-4.8.6 && ./configure -platform unsupported/macx-clang -static -debug-and-release -qt-zlib -qt-libpng -qt-libmng -qt-libtiff -qt-libjpeg -opensource -confirm-license -no-webkit -no-phonon -no-phonon-backend -no-script -no-scripttools -no-qt3support -no-multimedia -no-dbus -no-pch -nomake demos -nomake tools -nomake examples )
	(cd qt-everywhere-opensource-src-4.8.6 && make $(BUILD_FASTER_FLAGS) )
	@echo "***************"
	@echo " Done!"
	@echo " Run the following commands to complete install"
	@echo "  cd qt-everywhere-opensource-src-4.8.6"
	@echo "  # Install into  /usr/local/Trolltech"
	@echo "  sudo make install"
	@echo "  # copy the Qt sources to /usr/local/Trolltech"
	@echo "  cd .."
	@echo "  sudo cp -R qt-everywhere-opensource-src-4.8.6 /usr/local/Trolltech"
	@echo "***************"



openssl-1.0.1g/.done:
	$(MAKE) $(INSTALLDIR)
	rm -rf openssl-1.0.1e
	tar xzf openssl-1.0.1g.tar.gz
	(cd openssl-1.0.1g && $(SSL_CONFIG_COMMAND) no-krb5 -DPURIFY --prefix=$(INSTALLDIR) no-shared)
	(cd openssl-1.0.1g && make)
	(cd openssl-1.0.1g && make install_sw)
	# make symlinks to the libraries so we can control which one we use
	(cd install/lib && ln -s -f libssl.a libnutonian_ssl.a)
	(cd install/lib && ln -s -f libcrypto.a libnutonian_crypto.a)
	touch openssl-1.0.1g/.done


# Since we check in gcovr directly, this target shouldn't need to run
# it is included here mostly for documentation of where the script
# came from
gcovr:
	 wget https://software.sandia.gov/trac/fast/export/2825/gcovr/trunk/scripts/gcovr

clean:
	rm -rf  boost_1_52_0 boost_1_55_0 boost_1_57_0 install qt-everywhere-opensource-src-4.8.6
